<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "nama : ". $sheep->name. "<br>";
echo "legs : ". $sheep-> legs. "<br>";
echo "cool blooded : ".$sheep->cool_blooded."<br><br>";

$kodok = new frog('buduk');
echo "nama : ". $kodok->name. "<br>";
echo "legs : ". $kodok-> legs. "<br>";
echo "cool blooded : ".$kodok->cool_blooded."<br>";
echo "jump : ". $kodok-> jump. "<br><br>";

$sungokong = new ape("kera sakti");
echo "nama : ". $sungokong->name. "<br>";
echo "legs : ". $sungokong-> legs. "<br>";
echo "cool blooded : ".$sungokong->cool_blooded."<br>";
echo "yell : ". $sungokong-> yell. "<br><br>";